import javax.swing.*;

public class OlympicRingViewer{
    public static void main(String[] args) {
    //Construct and initialize a frame
        JFrame frame = new JFrame();
        frame. setSize(500,400);
        frame.setTitle("Olympic Rings - E3.24");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // Add the component to the frame
        OlympicRingComponent component = new OlympicRingComponent(30, 30,100);
        frame.add(component);
        // Make the frame visible
        frame.setVisible(true);
    }
}
