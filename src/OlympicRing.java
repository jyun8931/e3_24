import java.awt.*;
import java.awt.geom.Ellipse2D;

public class OlympicRing {
    private int xLeft;
    private int yTop;
    private int radius;
    Color color;
    /* Construct an OlympicRing based on position, radius and color */
    public OlympicRing(int x, int y, int r, Color color){
        xLeft = x;
        yTop = y;
        radius = r;
        this.color = color;
    }
    /* Draws a ring */
    public void draw(Graphics2D g2){
        Ellipse2D.Double ring = new Ellipse2D.Double(xLeft, yTop, radius, radius);
        g2.setStroke(new BasicStroke(10));
        g2.setColor(color);
        g2.draw(ring);
    }
}
