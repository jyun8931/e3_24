import javax.swing.*;
import java.awt.*;

public class OlympicRingComponent extends JComponent {
    private int xLeft, yTop, radius;

    /* Construct Olympic rings based on position and radius of each
    ring */
    public OlympicRingComponent(int x, int y, int r){
        xLeft = x;
        yTop = y;
        radius = r;
    }

    /**
     * Draws multiple rings with different colors
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        //Drawing instructions goes here

        OlympicRing ring1 = new OlympicRing(xLeft, yTop, radius, Color.BLUE);
        ring1.draw(g2);
        OlympicRing ring3 = new OlympicRing(xLeft + radius +10, yTop +0, radius, Color.BLACK);
        ring3.draw(g2);
        OlympicRing ring5 = new OlympicRing(xLeft + radius*2 + 20, yTop +0, radius, Color.RED);
        ring5.draw(g2);
        OlympicRing ring2 = new OlympicRing(xLeft + radius/2+5, yTop +radius/2, radius, Color.yellow);
        ring2.draw(g2);
        OlympicRing ring4 = new OlympicRing(xLeft + radius*3/2+15, yTop +radius/2, radius, Color.GREEN);
        ring4.draw(g2);

    }
}
